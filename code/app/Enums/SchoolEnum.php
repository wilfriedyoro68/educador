<?php

namespace App\Enums;

enum SchoolEnum : string
{

    CASE SCHOOL_1 = "school_1";
    CASE SCHOOL_2 = "school_2";
    CASE SCHOOL_3 = "school_3";

}
