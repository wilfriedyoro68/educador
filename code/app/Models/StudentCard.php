<?php

namespace App\Models;

use App\Enums\SchoolEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class StudentCard extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'school',
        'is_internal',
        'birthday',
        'description',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'school' => SchoolEnum::class,
        'is_internal' => 'boolean',
        'birthday' => 'date',
        'description',
    ];

    /**
     *
     * Get the user that owns the card.
     *
     * @return BelongsTo<User, StudentCard>
     *
     */
    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
